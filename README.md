# Welcome!

Welcome to the repository for our final project for Concurrent and Distributed
Systems! [Here](http://users.ece.utexas.edu/~garg/sp16-proj.html) is the link to
all of the different project descriptions.

# Assignment

For this project we will be comparing the performance of Hadoop vs Apache Spark
for a simple distributed application. The details of the assignment are listed
below.

## Using Spark for Distributed Processing

Hadoop requires multiple disk copies for processing intermediate key-value
pairs, especially when multiple iterations on data is required. Spark speeds up
distributed computation by doing more in-memory processing (and thus reducing
disk accesses). It is based around a datastructure called Resilient Distributed
Dataset (RDD). Implement the context word search program (Assignment 4) using
Spark and compare the performance with Hadoop. Information on Spark is
available [here](http://spark.apache.org).


# Results

The results for our project will be discussed [in our Final Project Paper
stored on Google Drive](https://drive.google.com/open?id=0B5UU08qsu-aYd1VKWnE3YV9JM0k).

# Original Homework 4 Assignment

In this assignment, you will implement a text analyzer using Hadoop, which is a
programming model and software framework for developing applications that
concurrently process large scale data (Big Data) on distributed systems. The
analyzer has to build a table that shows the occurrences of the words that
appear together in the given text. The data set for this problem is the novel
Pride and Prejudice. A collection of files (one per chapter) is provided in the
attached zip file for the assignment.  The occurrences is calculated in
individual lines; your program does not need to intelligently separate
sentences because the Mapper function is invoked for each newline by default.
Here is an example for calculating the occurrences:

Text: _"Mr. Bingley was good-looking and gentlemanlike; he had a pleasant
countenance, and easy, unaffected manners."_

Occurrences: For the word *Bingley*, _"and"_ appears twice
and _"gentlemanlike"_ appears once.  In this calculation, we say Bingley is a
'contextword'; _"and"_ and _"gentlemanlike"_ are 'querywords'.  The other example:
for the contextword *and*, _"pleasant"_ appears once, _"and"_ appears once (not zero
times or twice).

For simplicity of this assignment and obtaining the same
results, you should

1. convert every character into lower-case
2. mask non-word characters by white-space; i.e., any character other than a-z, A-Z, or
0-9 should be replaced by a single-space character. For instance, _"Bingley's
book"_ is converted into _"bingley s book"_. If the text of the novel is fully
analyzed, we should get these results:

*  _Darcy_ occurs 114 times with _Bingley_; _Bingley_ is the contextword and _Darcy_ is
   the queryword.
*  _Elizabeth_ occurs 146 times with _Jane_; _Jane_ is the contextword, and _Elizabeth_
   is the queryword.  Your Hadoop program has to output a table that contains
   all combinations of different contextwords and querywords. The table should
   follow the format (and only this format) of:


   contextword1

   <queryword1, occurrence>

   <queryword2, occurrence>


   contextword2

   <queryword1, occurrence>

   <queryword2, occurrence>


   where the querywords for the same contextword are sorted in their lexicographic order.

# HOW TO RUN


For instructions on how to run our hadoop and spark applications, please refer
to the Amazon EMR documentation for both
[Hadoop](https://aws.amazon.com/elasticmapreduce) and [Apache
Spark](http://aws.amazon.com/elasticmapreduce/details/spark).

