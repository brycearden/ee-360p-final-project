import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.NavigableSet;
import java.io.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Stephen and Bryce's Implementation of HW4.
 * This implementation of HW4 got a 100/100. If Jon and Phillip want to add
 * their implementation of HW4 into the respository as well then they can, but
 * I don't think that it is necessary.
 */

// Do not change the signature of this class
public class TextAnalyzer extends Configured implements Tool {

    // Replace "?" with your own output key / value types
    // The four template data types are:
    //     <Input Key Type, Input Value Type, Output Key Type, Output Value Type>
    public static class TextMapper extends
    Mapper<LongWritable,        // Input key type
    Text,                       // Input value type
    Text,                       // Output key type
    TextArrayWritable>              // Output value type
    {
        public void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException
        {
            String line = standardizeString(value.toString());
            String[] words = line.split("\\s+");
            ArrayList<String> wordsList = new ArrayList<String>(Arrays.asList(words));

            // Prevents adding duplicate words by converting to a Set
            HashSet<String> wordsMap = new HashSet<String>();

            for(int i = 0; i < words.length; i++) {

                if(!wordsMap.contains(words[i])) {

                    // Remove current word from query list
                    ArrayList<String> queryWordsList = new ArrayList<String>(wordsList);
                    queryWordsList.remove(i);

                    // Create array writable
                    String[] queryWordsArray = new String[queryWordsList.size()];
                    queryWordsArray = queryWordsList.toArray(queryWordsArray);
                    TextArrayWritable queryWords = new TextArrayWritable(queryWordsArray);

                    context.write(new Text(words[i]), queryWords);
                    wordsMap.add(words[i]);
                }
            }
        }

        // Converts every character into lower-case
        // Mask non-word characters by white-space
        private String standardizeString(String line) {
            return line.toLowerCase().replaceAll("\\W", " ");
        }

    }

    // Replace "?" with your own key / value types
    // NOTE: combiner's output key / value types have to be the same as those of mapper
    // We did not need to use a Combiner for this project since our number of
    // computations was less than 10,000,000 without one. However, if we do
    // need to use a Combiner at some point then this is the method stub.
    // public static class TextCombiner extends Reducer<?, ?, ?, ?> {
    //     public void reduce(Text key, Iterable<Tuple> tuples, Context context)
    //         throws IOException, InterruptedException
    //     {
    //         // Implementation of you combiner function
    //     }
    // }


    public static class TextReducer extends Reducer<Text, TextArrayWritable, Text, Text> {
        private final static Text emptyText = new Text("");

        // Gets a key from a mapper and iterates through all the ArrayWritables that we
        // passed. Counts up the occurances in a hashmap
        //
        // TODO: Need to convert the hashmap into a string that looks like that prompt
        // format
        public void reduce(Text key, Iterable<TextArrayWritable> queryWords, Context context)
            throws IOException, InterruptedException
        {

            // write out current Context key
            if(!key.toString().isEmpty()) {
                context.write(key, emptyText);

                // generate HashMap for all query words
                TreeMap<String, Integer> queryWordsMap = new TreeMap<String, Integer>();
                for(TextArrayWritable words : queryWords) {
                    String[] wordsArray = words.toStrings();
                    for(int i = 0; i < wordsArray.length; i++) {
                        String word = wordsArray[i];
                        if(!word.isEmpty()) {
                            if(queryWordsMap.containsKey(word)) {
                                queryWordsMap.put(word, queryWordsMap.get(word) + 1);
                            } else {
                                queryWordsMap.put(word, 1);
                            }
                        }
                    }
                }

                // write all Hashmap contents to the context
                Iterator<String> iterator = queryWordsMap.navigableKeySet().iterator();
                while(iterator.hasNext()) {
                    String word = iterator.next();
                    String cword = "<" + word + ",";
                    String count = queryWordsMap.get(word).toString() + ">";
                    context.write(new Text(cword), new Text(count));
                }

                // empty line for ending the current context key
                context.write(emptyText, emptyText);
                }
        }
    }

    public int run(String[] args) throws Exception {
        Configuration conf = this.getConf();

        // Create job
        Job job = new Job(conf, "BSA435_SMA2678"); // Replace with your EIDs
        job.setJarByClass(TextAnalyzer.class);

        // Setup MapReduce job
        job.setMapperClass(TextMapper.class);
        //   Uncomment the following line if you want to use Combiner class
        // job.setCombinerClass(TextCombiner.class);
        job.setReducerClass(TextReducer.class);

        // Specify key / value types (Don't change them for the purpose of this assignment)
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        //   If your mapper and combiner's  output types are different from Text.class,
        //   then uncomment the following lines to specify the data types.
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(TextArrayWritable.class);

        // Input
        FileInputFormat.addInputPath(job, new Path(args[0]));
        job.setInputFormatClass(TextInputFormat.class);

        // Output
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputFormatClass(TextOutputFormat.class);

        // Execute job and return status
        return job.waitForCompletion(true) ? 0 : 1;
    }

    // Do not modify the main method
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new TextAnalyzer(), args);
        System.exit(res);
    }

    public static class TextArrayWritable extends ArrayWritable {
        public TextArrayWritable() {
            super(Text.class);
        }

        public TextArrayWritable(Text[] strings) {
            super(Text.class, strings);
        }

        public TextArrayWritable(String[] strings) {
            super(Text.class);
            Text[] texts = new Text[strings.length];
            for (int i = 0; i < strings.length; i++) {
                texts[i] = new Text(strings[i]);
            }
            set(texts);
        }
    }
}

