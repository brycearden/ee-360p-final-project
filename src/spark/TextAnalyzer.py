from pyspark import SparkContext
import re
import argparse

def mapper(line):
    """Mapper for Apache Spark Program"""
    lower = line.lower()
    clean = re.sub('[^A-Za-z0-9]+', ' ', lower)
    words = clean.split()

    tuple_list = []
    words_map = {}

    for index, word in enumerate(words):
        if word not in words_map:
            query_words = list(words)
            query_words.pop(index)
            query_words_tuples = []
            # create a list of (query word, count) tuples
            for qw in query_words:
                query_words_tuples.append((qw, 1))
            tuple_list.append((word, query_words_tuples))
            words_map[word] = True
    return tuple_list

def aggregator(tup):
    """Aggregator for Apache Spark Program"""
    context_word, array_of_array_of_query_words = tup

    # count the number of appearances of each query word in the array of arrays
    count = {}
    for array_of_query_words in array_of_array_of_query_words:
        for query_word_tup in array_of_query_words:
            qw, _ = query_word_tup
            if qw in count:
                count[qw] += 1
            else:
                count[qw] = 1

    # sort the query words with their new counts into result
    result = []
    for key in sorted(count.iterkeys()):
        result.append((key, count[key]))

    return (context_word, result)

def formatter(tup):
    """Formatter for Apache Spark Program"""
    context_word, query_word_tuples = tup

    formatted_string = context_word + "\n"
    for qw_tup in query_word_tuples:
        qw, count = qw_tup
        formatted_string += ("<" + qw + ",\t" + str(count) + ">\n")

    return formatted_string

def main():
    """Runs the Apache Spark Context Word Program"""
    parser = argparse.ArgumentParser(
        description='Program Description: Spark context word program')
    parser.add_argument('inputDest',
                       metavar='inputDest',
                       type=str,
                       help='path to input data set for the python spark \
                       context word program')
    parser.add_argument('outputDest',
                       metavar='outputDest',
                       type=str,
                       help='path to output file for context word program')

    # parse args and setup Spark Application
    args = parser.parse_args()
    sc = SparkContext(appName="ContextQueryWords")

    # use mapper func to perform line to [(context_word, [query_words...]), ...]
    lines = sc.textFile(args.inputDest)
    mapper_output = lines.flatMap(mapper)

    # combine all arrays of query words that belong to the same key (i.e. context word)
    reducer_output = mapper_output.groupByKey().map(
        lambda tup: (tup[0], list(tup[1]))).sortByKey().map(
            lambda tup: aggregator(tup))

    # run each data item in reducer_output through formatter
    result = reducer_output.map(lambda tup: formatter(tup))

    # collect all data into one place and write it to disk
    result.coalesce(1, True).saveAsTextFile(args.outputDest)
    sc.stop()

if __name__ == "__main__":
    main()

